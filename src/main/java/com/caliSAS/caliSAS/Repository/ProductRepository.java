package com.caliSAS.caliSAS.Repository;

import com.caliSAS.caliSAS.Domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
