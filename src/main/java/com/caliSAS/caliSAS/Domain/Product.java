package com.caliSAS.caliSAS.Domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    @Column (columnDefinition = "serial")
     private long idProduct;

    @Column (length = 20)
     private String referenceProduct;

    @Column (length = 50)
     private String nameProduct;

    @Column(length = 20)
     private String date;

    @Column(length = 20)
     private String QuantityManufactured;


}
