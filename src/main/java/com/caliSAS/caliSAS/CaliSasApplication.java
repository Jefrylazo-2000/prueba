package com.caliSAS.caliSAS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaliSasApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaliSasApplication.class, args);
	}

}
