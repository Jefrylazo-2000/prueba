package com.caliSAS.caliSAS.Service;

import com.caliSAS.caliSAS.Domain.Product;
import com.caliSAS.caliSAS.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public interface IProductService {

    public Iterable<Product> read ();

    public Product create (Product product);

    public Product update (Product product);

    public void delete (Long id);

    public Optional <Product> getById (Long id);


}
