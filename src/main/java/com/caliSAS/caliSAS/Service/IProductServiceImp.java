package com.caliSAS.caliSAS.Service;

import com.caliSAS.caliSAS.Domain.Product;
import com.caliSAS.caliSAS.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IProductServiceImp implements IProductService {

    @Autowired
    ProductRepository productRepository ;

    @Override
    public Iterable<Product> read () {
        return productRepository.findAll();
    }

    @Override
    public Product create (Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product update (Product product) {
        return productRepository.save(product);
    }

    @Override
    public void delete (Long id) {
        productRepository.deleteById(id);
    }
    @Override
    public Optional<Product> getById (Long id) {
        return productRepository.findById(id);
    }
}
