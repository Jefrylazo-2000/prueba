package com.caliSAS.caliSAS.web.rest;


import com.caliSAS.caliSAS.Domain.Product;
import com.caliSAS.caliSAS.Service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping
public class ProductResource {

    @Autowired
    IProductService productService;

    @GetMapping("/product")
    public Iterable<Product> read() {
        return productService.read();
    }

    @PostMapping("/product")
    public Product create(@RequestBody Product product){
        return productService.create(product);
    }
    @PutMapping
    public Product update (@RequestBody Product product){
        return productService.create(product);
    }

    @DeleteMapping("/product/{id}")
    public void delete (@PathVariable Long id){
        productService.delete(id);
    }

    @GetMapping("/product/{id}")
    public Optional<Product> getById (@PathVariable Long id){
        return productService.getById(id);
    }
}
